import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Text, Card } from 'react-native-elements';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import styles from '../styles/index';

import * as Actions from '../actions'; 

class UserDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: `Details for ${navigation.getParam('user', 'N/A').first_name}`,
    };
  };

  deleteUser = (currentUser) => {
    this.props.removeUser(currentUser);
    this.props.navigation.goBack();
  }
  
  render() {
    const { navigation } = this.props;
    const user = navigation.getParam('user', 'N/A');
    return (
      <View style={styles.container}>
        <Card
          title={`${user.first_name} ${user.last_name}`}
          image={{uri: user.image_url}}>
          <Text style={{ marginBottom: 20 }}>{user.bio}</Text> 
          <Button
            raised
            style={{ marginBottom: 10 }}
            title="Delete user"
            backgroundColor="red"
            onPress={() => this.deleteUser(user)}
          />
          </Card>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
      loading: state.dataReducer.loading,
      users: state.dataReducer.users
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);