import React, { Component } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  ScrollView,
  FlatList,
  Picker,
  ActivityIndicator
} from 'react-native';
import { List, ListItem, Button } from 'react-native-elements'

import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../actions'; 
import styles from '../styles/index';

class Main extends Component {
  static navigationOptions = {
    title: 'User administration',
  };

  componentDidMount() {
    this.props.getData();
  }

  render() {
    if (this.props.loading) {
      return (
        <View style={styles.activityIndicatorContainer}>
            <ActivityIndicator animating={true}/>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <ScrollView>
        <List>
          {
            this.props.users.map((item) => (
              <ListItem
                style={styles.listItem}
                roundAvatar
                avatar={{uri:item.image_url}}
                key={item.key}
                title={`${item.first_name} ${item.last_name}`}
                onPress={ () => this.props.navigation.navigate('userDetails', { user: item }) }
              />
            ))
          }
        </List>
        </ScrollView>
        <Button
          style={{ marginBottom: 10, marginTop: 10 }}
          backgroundColor="green"
          title="Add User"
          onPress={() => this.props.navigation.navigate('userCreate')}
        />
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
      loading: state.dataReducer.loading,
      users: state.dataReducer.users
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);