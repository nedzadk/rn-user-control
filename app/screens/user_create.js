import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { FormLabel, FormInput, FormValidationMessage, Button } from 'react-native-elements'
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import styles from '../styles/index';

import * as Actions from '../actions'; 


class UserCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      bio: '',
      fError: false,
      lError: false,
      bError: false
    }
  }
  static navigationOptions = {
    title: 'Create new user',
  };

  onFirstNameChange = (text) => {
    this.setState({ first_name: text});
  }

  onLastNameChange = (text) => {
    this.setState({ last_name: text});
  }

  onBioChange = (text) => {
    this.setState({ bio: text});
  }

  submitForm = () => {
    let { first_name, last_name, bio } = this.state;
    let fE = false;
    let lE = false;
    let bE = false;
    if (first_name.length < 2) {
      fE = true;
    }
    if (last_name.length < 2) {
      lE = true;
    }
    if (bio.length < 2) {
      bE = true;
    }
    if (fE || lE || bE) {
      return this.setState({ fError: fE, lError: lE, bError: bE});
    }

    this.props.createUser({ first_name, last_name, bio });
    this.props.navigation.goBack();
  }

  renderError = (error) => {
    if (error) 
      return <FormValidationMessage>This field is required</FormValidationMessage>
  }

  render() {
    return (
      <View>
        <FormLabel>First name</FormLabel>
        <FormInput
          onChangeText={(text) => this.onFirstNameChange(text)}
          style={styles.input}
          placeholder="First name"
        />
        {this.renderError(this.state.fError)}

        <FormLabel>Last name</FormLabel>
        <FormInput
          onChangeText={(text) => this.onLastNameChange(text)}
          style={styles.input}
          placeholder="Last name"
        />
        {this.renderError(this.state.lError)}

        <FormLabel>Short bio</FormLabel>
        <FormInput
          onChangeText={(text) => this.onBioChange(text)}
          style={styles.input}
          placeholder="Short bio"
        />
        {this.renderError(this.state.bError)}

        <Button 
          raised
          backgroundColor="green"
          style={{ paddingTop: 20 }}
          title="Save user"
          onPress={ () => this.submitForm() }/>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
      loading: state.dataReducer.loading,
      data: state.dataReducer.data
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UserCreate);

