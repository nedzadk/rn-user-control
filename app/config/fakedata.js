import faker from 'faker';
export const fakeInitialData = [
  {key: '1', image_url: faker.image.avatar(), first_name: 'Nedzad', last_name: 'Kulelija', bio: 'Very very smart dude'}, 
  {key: '2', image_url: faker.image.avatar(), first_name: 'Mircea', last_name: 'Tirea', bio: 'Thinks that Nedzad is awesome and should work for Upstack :)'},
];