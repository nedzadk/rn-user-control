import { combineReducers } from 'redux';
 
import { DATA_FETCHED, USER_DELETED, USER_ADDED } from '../actions/';
 
let dataState = { data: [], loading:true };
 
const dataReducer = (state = dataState, action) => {
    switch (action.type) {
        case DATA_FETCHED:
            state = Object.assign({}, state, { users: action.data, loading: false });
            return state;
        case USER_ADDED:
        state = Object.assign({}, state, { users: action.data, loading: false });
            return state;
        case USER_DELETED:
            state = Object.assign({}, state, { users: action.data, loading: false });
            return state;
        default:
            return state;
    }
};
 
const rootReducer = combineReducers({
    dataReducer
})
 
export default rootReducer;
