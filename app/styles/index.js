import { StyleSheet } from 'react-native';
export default StyleSheet.create({
  container: {
    flex: 1,
  },
  
  header: {
    fontSize: 24,
    padding: 10,
  },

  input: {
    margin: 15,
    height: 40,
    borderColor: 'black',
    borderWidth: 1
 },
 listItem: {
   flex: 1
 },
 userDetails: {
   fontSize: 20,
   padding: 10
 }
 
});