import { AsyncStorage } from 'react-native';
import faker from 'faker';

export const DATA_FETCHED = 'FETCHED_DATA';
export const USER_DELETED = 'USER_DELETED';
export const USER_ADDED = 'USER_ADDED';

export function getData() {
    return (dispatch) => {
        AsyncStorage.getItem('fakeData')
            .then((data) => {
                dispatch(fetchedData(JSON.parse(data)));
            });
    };
};

export function fetchedData(fData) {
    return {type: DATA_FETCHED, data:fData}
}

export function createUser(newUser) {
    return (dispatch) => {
        AsyncStorage.getItem('fakeData')
        .then((data) => {
            let users = JSON.parse(data);
            users.push({
                key: faker.random.number(),
                image_url: faker.image.avatar(),
                ...newUser
            });
            return users;
        })
        .then((data) => {
            return AsyncStorage.setItem('fakeData', JSON.stringify(data));
        })
        .then(() => {
            this.getData();
        })
    }
}

export function removeUser(userToRemove) {
    return (dispatch) => {
        AsyncStorage.getItem('fakeData')
            .then((data) => {
                let users = JSON.parse(data);
                const index = users.findIndex(user => user.key === userToRemove.key);
                if (index !== -1) users.splice(index, 1);
                return users;
            })
            .then((data) => {
                return AsyncStorage.setItem('fakeData', JSON.stringify(data));
            })
            .then(() => {
                this.getData();
            })
    }
}
