import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';

// Screens
import Main from './app/screens/main';
import UserCreate from './app/screens/user_create'; 
import UserDetails from './app/screens/user_details';
// Store
import store from './app/stores/index';
// Fake initial data
import { fakeInitialData } from './app/config/fakedata';
AsyncStorage.setItem('fakeData', JSON.stringify(fakeInitialData));

const AppNavigation = createStackNavigator(
  {
    main: Main,
    userCreate: UserCreate,
    userDetails: UserDetails,

  }, {
    initialRouteName: 'main',
  }
);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppNavigation/>
      </Provider>
    );
  }
}


